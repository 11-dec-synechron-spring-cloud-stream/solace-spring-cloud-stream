package com.classpath.solacespringcloudbinder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.UUID;
import java.util.function.Function;
import java.util.function.Supplier;

@SpringBootApplication
public class SolaceSpringCloudBinderApplication {

    public static void main(String[] args) {
        SpringApplication.run(SolaceSpringCloudBinderApplication.class, args);
    }

    @Bean
    public Supplier<String> generateData(){
        return () -> UUID.randomUUID().toString();
    }
}
